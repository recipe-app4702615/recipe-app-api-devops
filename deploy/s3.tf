resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files-442077926023"
  acl           = "public-read"
  force_destroy = true
}

# This is to add ability to add account number to the name of the bucket
# main.tf: data "aws_caller_identity" "current" {}
# s3.tf: bucket        = "${local.prefix}-files-${data.aws_caller_identity.current.account_id}"
