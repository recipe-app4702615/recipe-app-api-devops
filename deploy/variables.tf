variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "hladha@training.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance" # set via gitlab variable TF_VAR_db_username
}

variable "db_password" {
  description = "Password for the RDS postgres instance" # set via gitlab variable TF_VAR_db_password
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "442077926023.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest" # taken from console
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "442077926023.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest" # taken from console
}

variable "django_secret_key" {
  description = "Secret key for Django app" # set via gitlab variable TF_VAR_django_secret_key
}
