# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development


## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000

# Prerequisites to setup the CICD:

## S3 bucket:
 - recipe-app-api-devops-tfstate-<account-number>
 - DyanamoDB Table: recipe-app-api-devops-tf-state-lock

## ECR Repositories:
 - recipe-app-api-devops

 -- will need to be updated in gitlab variables

## IAM:
 - RecipeAppApi-CI (policy: RecipeAppApi-CI)

```

policy: RecipeAppApi-CI
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "TerraformRequiredPermissions",
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ec2:*",
                "rds:DeleteDBSubnetGroup",
                "rds:CreateDBInstance",
                "rds:CreateDBSubnetGroup",
                "rds:DeleteDBInstance",
                "rds:DescribeDBSubnetGroups",
                "rds:DescribeDBInstances",
                "rds:ListTagsForResource",
                "rds:ModifyDBInstance",
                "iam:CreateServiceLinkedRole",
                "rds:AddTagsToResource",
                "iam:CreateRole",
                "iam:GetInstanceProfile",
                "iam:DeletePolicy",
                "iam:DetachRolePolicy",
                "iam:GetRole",
                "iam:AddRoleToInstanceProfile",
                "iam:ListInstanceProfilesForRole",
                "iam:ListAttachedRolePolicies",
                "iam:DeleteRole",
                "iam:TagRole",
                "iam:PassRole",
                "iam:GetPolicyVersion",
                "iam:GetPolicy",
                "iam:CreatePolicyVersion",
                "iam:DeletePolicyVersion",
                "iam:CreateInstanceProfile",
                "iam:DeleteInstanceProfile",
                "iam:ListPolicyVersions",
                "iam:AttachRolePolicy",
                "iam:CreatePolicy",
                "iam:RemoveRoleFromInstanceProfile",
                "logs:CreateLogGroup",
                "logs:DeleteLogGroup",
                "logs:DescribeLogGroups",
                "logs:ListTagsLogGroup",
                "logs:TagLogGroup",
                "ecs:DeleteCluster",
                "ecs:CreateService",
                "ecs:UpdateService",
                "ecs:DeregisterTaskDefinition",
                "ecs:DescribeClusters",
                "ecs:RegisterTaskDefinition",
                "ecs:DeleteService",
                "ecs:DescribeTaskDefinition",
                "ecs:DescribeServices",
                "ecs:CreateCluster",
                "elasticloadbalancing:*",
                "s3:*"
            ],
            "Resource": "*"
        },
        {
            "Sid": "AllowListS3StateBucket",
            "Effect": "Allow",
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::recipe-app-api-devops-tfstate-<account-number>"
        },
        {
            "Sid": "AllowS3StateBucketAccess",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Resource": "arn:aws:s3:::recipe-app-api-devops-tfstate-<account-number>/*"
        },
        {
            "Sid": "LimitEC2Size",
            "Effect": "Deny",
            "Action": "ec2:RunInstances",
            "Resource": "arn:aws:ec2:*:*:instance/*",
            "Condition": {
                "ForAnyValue:StringNotLike": {
                    "ec2:InstanceType": [
                        "t2.micro"
                    ]
                }
            }
        },
        {
            "Sid": "AllowECRAccess",
            "Effect": "Allow",
            "Action": [
                "ecr:*"
            ],
            "Resource": "arn:aws:ecr:us-east-1:*:repository/recipe-app-api-devops"
        },
        {
            "Sid": "AllowStateLockingAccess",
            "Effect": "Allow",
            "Action": [
                "dynamodb:PutItem",
                "dynamodb:DeleteItem",
                "dynamodb:GetItem"
            ],
            "Resource": [
                "arn:aws:dynamodb:*:*:table/recipe-app-api-devops-tf-state-lock"
            ]
        }
    ]
}
```

